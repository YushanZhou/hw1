package tdlm.controller;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;
import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import static saf.settings.AppPropertyType.NEW_COMPLETED_MESSAGE;
import static saf.settings.AppPropertyType.NEW_COMPLETED_TITLE;
import saf.ui.AppGUI;
import saf.ui.AppItemDialog;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.data.ToDoItem;
import static saf.settings.AppPropertyType.ADD_ITEM_TITLE;
import static saf.settings.AppPropertyType.EDIT_ITEM_TITLE;
import static saf.settings.AppPropertyType.REMOVE_MESSAGE;
import static saf.settings.AppPropertyType.REMOVE_TITLE;
import static tdlm.PropertyType.CATEGORY_COLUMN_HEADING;
import static tdlm.PropertyType.COMPLETED_COLUMN_HEADING;
import static tdlm.PropertyType.DESCRIPTION_COLUMN_HEADING;
import static tdlm.PropertyType.END_DATE_COLUMN_HEADING;
import static tdlm.PropertyType.START_DATE_COLUMN_HEADING;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla
 * @author Yushan Zhou
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    DataManager dataManager;
       
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
        try {
            dataManager = (DataManager)app.getDataComponent();
        } catch (Exception ex) {
            Logger.getLogger(ToDoListController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        
        
        AppItemDialog dialog = AppItemDialog.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        dialog.showLabelMessage(props.getProperty(ADD_ITEM_TITLE),
                                props.getProperty(DESCRIPTION_COLUMN_HEADING),
                                props.getProperty(START_DATE_COLUMN_HEADING),
                                props.getProperty(END_DATE_COLUMN_HEADING),
                                props.getProperty(COMPLETED_COLUMN_HEADING));
        
        dialog.getCategoryTextField().setText(null);
        dialog.getDescriptionTextField().setText(null);
        dialog.getStartDatePicker().setValue(null);
        dialog.getEndDatePicker().setValue(null);
        dialog.getCheckBox().setSelected(false);
        
        dialog.show(props.getProperty(ADD_ITEM_TITLE));
            
        if(dialog.saveItem()){
            //... if you edit the contents and click ok, you can save again
            app.getGUI().markUnsavedAndUpdateToolbar();
            
            ToDoItem item = dialog.addNewItem();
            
            ToDoItem newItem = new ToDoItem();
            newItem.setCategory(item.getCategory());
            newItem.setDescription(item.getDescription());
            newItem.setStartDate(item.getStartDate());
            newItem.setEndDate(item.getEndDate());
            newItem.setCompleted(item.getCompleted());
            dataManager.addItem(newItem);
            
            workspace.makeClickable();
        }
    }
    
    public void processRemoveItem() {
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        
        AppYesNoCancelDialogSingleton comfirm = AppYesNoCancelDialogSingleton.getSingleton();
        comfirm.show(props.getProperty(REMOVE_TITLE), props.getProperty(REMOVE_MESSAGE));
        
        if(comfirm.getSelection().equals(AppYesNoCancelDialogSingleton.YES)){
            dataManager.getItems().remove(workspace.getTable().getSelectionModel().getSelectedItem());
        }
        
        // ... if you edit the contents and click ok, you can save again
        app.getGUI().markUnsavedAndUpdateToolbar();
        
        if(dataManager.getItems().size() == 0) {
            workspace.makeUnclickable();
        }
    }
    
    public void processMoveUpItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        
        dataManager.moveUpItem(workspace.getTable().getSelectionModel().getSelectedItem());
        
        // ... if you edit the contents and click ok, you can save again
        app.getGUI().markUnsavedAndUpdateToolbar();
    }
    
    public void processMoveDownItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        
        dataManager.moveDownItem(workspace.getTable().getSelectionModel().getSelectedItem());
        
        // ... if you edit the contents and click ok, you can save again
        app.getGUI().markUnsavedAndUpdateToolbar();
    }
    
    public void processEditItem() {
        // ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
	workspace.reloadWorkspace();
        
        ToDoItem item = workspace.getTable().getSelectionModel().getSelectedItem();
        
        AppItemDialog dialog = AppItemDialog.getSingleton();
        PropertiesManager props = PropertiesManager.getPropertiesManager();
        dialog.showLabelMessage(props.getProperty(ADD_ITEM_TITLE),
                                props.getProperty(DESCRIPTION_COLUMN_HEADING),
                                props.getProperty(START_DATE_COLUMN_HEADING),
                                props.getProperty(END_DATE_COLUMN_HEADING),
                                props.getProperty(COMPLETED_COLUMN_HEADING));
        
        dialog.getCategoryTextField().setText(item.getCategory());
        dialog.getDescriptionTextField().setText(item.getDescription());
        dialog.getStartDatePicker().setValue(item.getStartDate());
        dialog.getEndDatePicker().setValue(item.getEndDate());
        dialog.getCheckBox().setSelected(item.getCompleted());
        
        dialog.show(props.getProperty(EDIT_ITEM_TITLE));
        
        if(dialog.saveItem()){
            // save again
            app.getGUI().markUnsavedAndUpdateToolbar();
            
            ToDoItem editedItem = dialog.addNewItem();
            
            dataManager.getItems().get(dataManager.getItems().indexOf(item)).setCategory(editedItem.getCategory());
            dataManager.getItems().get(dataManager.getItems().indexOf(item)).setDescription(editedItem.getDescription());
            dataManager.getItems().get(dataManager.getItems().indexOf(item)).setStartDate(editedItem.getStartDate());
            dataManager.getItems().get(dataManager.getItems().indexOf(item)).setEndDate(editedItem.getEndDate());
            dataManager.getItems().get(dataManager.getItems().indexOf(item)).setCompleted(editedItem.getCompleted());
            
        }
    }
    
    public void updateUpDownButton() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        int index = dataManager.getItems().indexOf(workspace.getTable().getSelectionModel().getSelectedItem());
        
        if(dataManager.getItems().size() == 0){
            workspace.getMoveUpButton().setDisable(true);
            workspace.getMoveDownButton().setDisable(true);  
        } else if(index == 0 && index == (dataManager.getItems().size() - 1)) {
            workspace.getMoveUpButton().setDisable(true);
            workspace.getMoveDownButton().setDisable(true);
        }else if(index == 0){
            workspace.getMoveUpButton().setDisable(true);
            workspace.getMoveDownButton().setDisable(false);
        }else if(index == (dataManager.getItems().size() - 1)){
            workspace.getMoveUpButton().setDisable(false);
            workspace.getMoveDownButton().setDisable(true);
        }else if(index > 0 && index < (dataManager.getItems().size() - 1)) {
            workspace.getMoveUpButton().setDisable(false);
            workspace.getMoveDownButton().setDisable(false);
        }
    }
}
