package tdlm.data;

import java.util.ArrayList;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.effect.Effect;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Shape;
import tdlm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author Yushan Zhou
 * @version 1.0
 */
public class DataManager implements AppDataComponent {
    // FIRST THE THINGS THAT HAVE TO BE SAVED TO FILES
    
    // NAME OF THE TODO LIST
    StringProperty name;
    
    // LIST OWNER
    StringProperty owner;
    
    // THESE ARE THE ITEMS IN THE TODO LIST
    ObservableList<ToDoItem> items;
    
    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    
    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
	// KEEP THE APP FOR LATER
	app = initApp;
        
        items = FXCollections.observableArrayList();
        
        name = new SimpleStringProperty(null);
        owner = new SimpleStringProperty(null);
    }
    
    public ObservableList<ToDoItem> getItems() {
	return items;
    }
    
    public String getName() {
        return name.get();
    }
    
    
    public String getOwner() {
        return owner.get();
    }
    
    public void setName(String newName) {
        name.set(newName);
    }
    
    
    public void setOwner(String newOwner) {
        owner.set(newOwner);
    }
    
    
    public void addItem(ToDoItem item) {
        items.add(item);
    }



    /**
     * 
     */
    @Override
    public void reset() {
        name.set(null);
        owner.set(null);
        items.clear();
    }
    
    public void moveUpItem(ToDoItem item) {
        int index = items.indexOf(item);
        if (index > 0) {
            ToDoItem temp = items.get(index - 1);
            items.set(index-1, item);
            items.set(index, temp);
        }
    }
    
    public void moveDownItem(ToDoItem item){
        int index = items.indexOf(item);
        if (index < (items.size()-1)) {
            ToDoItem temp = items.get(index + 1);
            items.set(index+1, item);
            items.set(index, temp);
        }
    }
}
